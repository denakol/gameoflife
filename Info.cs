﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class Info
    {
        public Int32 Start { get; set; }
        public Int32 End { get; set; }
    }
}
