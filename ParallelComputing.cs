﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class SimultaneousStart
    {
        private Int32 _countThread =0;
        private ManualResetEvent eventEnd = new ManualResetEvent(false);
        private ManualResetEvent eventStart = new ManualResetEvent(false);

        public void AddTask(MyTask task, Int32 numberOfThread)
        {
            Action<Object> action = (infoObject) =>
            {
                var info = (MyTask)infoObject;
                //ждем события о начале
                eventStart.WaitOne();
                //выполняем функцию которую передали в задаче
                info.action(info.infoObject);
                //один поток выполнил работу
                Interlocked.Decrement(ref _countThread);
                if (_countThread == 0)
                {
                    //потоки завершили работу
                    eventEnd.Set();
                }
            };
            var thread = new Thread(new ParameterizedThreadStart(action));
            Interlocked.Increment(ref _countThread);
            thread.Start(task);
        }

        public void Start()
        {
            //начинаем работу
            eventStart.Set();
            //ждем окончания
            eventEnd.WaitOne();
            eventEnd.Reset();
            eventStart.Reset();
        }
    }
}
