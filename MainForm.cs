﻿

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections;

namespace GameOfLife
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            label2.Text = "1";
            FormClosing += MainForm_Closing;
        }

        private Int32 ProcessorCount = 1;
        /// <summary>Used to cancel the current game.</summary>
        private CancellationTokenSource _cancellation;
        /// <summary>The current game.</summary>
        private GameBoard _game;

        private static Dictionary<Int32, Double> points = new Dictionary<int, double>();

        private void chkParallel_CheckedChanged(object sender, EventArgs e)
        {
            if (_game != null) _game.UseThreadPool = chkParallel.Checked;
        }

        /// <summary>Run a game.</summary>
        private void btnRun_Click(object sender, EventArgs e)
        {
            // If no game is currently running, run one
            if (_cancellation == null)
            {
                // Clear the current image, get the size of the board to use, initialize cancellation,
                // and prepare the form for game running.
                pictureBox1.Image = null;
                int width = pictureBox1.Width, height = pictureBox1.Height;
                _cancellation = new CancellationTokenSource();
                var token = _cancellation.Token;
                lblDensity.Visible = false;
                tbDensity.Visible = false;
                btnRun.Text = "Stop";
                double initialDensity = tbDensity.Value / 1000.0;

                // Initialize the object pool and the game board
                var pool = new ObjectPool<Bitmap>(() => new Bitmap(width, height));
                _game = new GameBoard(width, height, initialDensity, pool) { UseThreadPool = chkParallel.Checked, NumberOfThread = ProcessorCount };

                // Run the game on a background thread
                //поток для обсчета фреймов игры
                Task.Factory.StartNew(() =>
                {
                    // Run until cancellation is requested
                    var sw = new Stopwatch();
                    while (!token.IsCancellationRequested)
                    {
                        // Move to the next board, timing how long it takes
                        sw.Restart();
                        Bitmap bmp = _game.MoveNext();
                        var framesPerSecond = 1 / sw.Elapsed.TotalSeconds;
                        
                        // Update the UI with the new board image
                        //вызываем функцию в потококе UI чтобы иметь доступ к интерфейсу
                        if (!token.IsCancellationRequested)
                        {
                            BeginInvoke((Action)(() =>
                            {
                                ChartPerfomance.Legends.Clear();
                                lblFramesPerSecond.Text = String.Format("Кадры / Секунда: {0:F2}", framesPerSecond);
                                var old = (Bitmap)pictureBox1.Image;
                                pictureBox1.Image = bmp;
                                if (points.ContainsKey(_game.NumberOfThread))
                                {
                                    points[_game.NumberOfThread] = (points[_game.NumberOfThread] +framesPerSecond)/2;
                                }
                                else
                                {
                                    points[_game.NumberOfThread] = framesPerSecond;
                                }
                                
                                var array = points.ToArray().OrderByDescending(x => x.Key);
                                var listX = array.Select(x => x.Key).ToList();
                                var lisY = array.Select(x => x.Value).ToList();
                                ChartPerfomance.Series[0].Points.DataBindXY(listX, "dfdsf", lisY, "dfgdfg");
                                ChartPerfomance.Series[0].ChartType = SeriesChartType.Area;
                                ChartPerfomance.Series[0]["BarLabelStyle"] = "Center";
                                ChartPerfomance.ChartAreas[0].Area3DStyle.Enable3D = true;
                      
                                if (old != null) pool.PutObject(old);
                            }));
                        }
                        
                    }

                    // When the game is done, reset the board.
                }, token).ContinueWith(_ =>
                {
                    _cancellation = null;
                    btnRun.Text = "Start";
                    lblDensity.Visible = true;
                    tbDensity.Visible = true;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }

                // If a game is currently running, cancel it
            else _cancellation.Cancel();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (_game != null)
            {
                _game.NumberOfThread = trackBar1.Value;
            };
            ProcessorCount = trackBar1.Value;
            label2.Text = trackBar1.Value.ToString();
        }

        private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _cancellation.Cancel();
        }
    }
}
